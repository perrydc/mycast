import npr, json, requests, pprint, re, time, sys
from subprocess import Popen, PIPE, STDOUT
orgId = sys.argv[1]
#orgId = 2 # San Francisco

# fetch STATION METADATA from npr
s = npr.Station(int(orgId))
try:
    stationName = s.response['attributes']['brand']['name']
    city = s.response['attributes']['brand']['marketCity']
    eligibleStations = json.load(open('eligible.json'))
    zip = eligibleStations[str(orgId)]['zip']
    lat = eligibleStations[str(orgId)]['lat']
    lon = eligibleStations[str(orgId)]['lon']
    print(lat,lon)
except:
    print("That station is not available / eligible for a mycast.")

# fetch TEMPERATURE and FORECAST from weather.gov
weatherUrl = "https://api.weather.gov/points/" + lat + ",-" + lon + "/forecast" #add /hourly for more detail
response = requests.get(weatherUrl).text
obj = json.loads(response)
temp = obj['properties']['periods'][0]['temperature']
shortForecast = obj['properties']['periods'][0]['shortForecast']
print(temp,shortForecast)

# construct SCRIPT for ffmpeg
skyCondition = 'none'
if re.search(r'Rain',shortForecast):
    skyCondition = 'rainy'
if re.search(r'Snow',shortForecast):
    skyCondition = 'snowy'
if re.search(r'Sun',shortForecast):
    skyCondition = 'sunny'
if re.search(r'Cloud',shortForecast):
    skyCondition = 'cloudy'
    
recordScript = "file 'temperature/" + str(temp) + ".mp4'\n"
recordScript += "file 'sky/" + skyCondition + ".mp4'\n"
recordScript += "file 'station/" + str(orgId) + ".mp4'"
fd = open('audio/list.txt','w')
fd.write(recordScript)
fd.close()
command = "ffmpeg -f concat -i audio/list.txt -y -c copy audio/cache/" + str(orgId) + ".mp4"
print(command)

# construct cash/orgId.mp4 from ffmpeg
commands = []
commands.append(command)
if(commands):
    processes = [Popen(cmd, shell=True, stdout=PIPE) for cmd in commands]
    for p in processes: 
        stdout = p.communicate()