# mycast

mycast is a **personalized newscast** that reads out the daily news 
and information that is most important to a listener.  The content 
may include:

* [weather](https://api.weather.gov/points/39.0693,-94.6716/forecast/hourly)
* financial data from the listeners' portfolio
* top social posts from a listeners' friends
* traffic over the listeners' regular commute
* story promos that would interest the listener
* alerts about school closings, new episodes in subscribed podcasts, etc.

## requirements:
* [ffmpeg](https://trac.ffmpeg.org/wiki/CompilationGuide/macOS)
* [pip install npr](https://github.com/perrydc/npr)

## testing proposal:
We will deploy this on NPR One in place of the local newscast for those stations 
that do not currently have a newscast.  Currently [half](http://mpxingest.publicbroadcasting.net/dashboard/station-content) of 
participating networks have submitted a viable newscast in the past year, or 
about 113 out of 234.  In those markets that have never submitted a newscast, 
users will hear:

> Currently it's **44** degrees under **cloudy** skies in **Marfa**

Depending on how this tests, the program will be expanded to markets that only 
infrequently upload a newscast as a default fallback.  Currently only about 
half of stations that submit a newscast do so on a daily basis (or 25%) of all 
participating organizations.

## process for building the **eligible.json** file:

> SELECT org_id,org_zip_code,org_longitude,org_latitude FROM API.organization 
WHERE org_latitude IS NOT NULL AND org_id BETWEEN 1 and 1570;

* Export as csv, then remove all orgs listed [here](http://mpxingest.publicbroadcasting.net/dashboard/station-content) via cut & paste and VLOOKUP
* Convert to serialized JSON via:

```python
import csv, json, pprint
stations = {}
with open("eligible.csv","rt") as source:
    rdr = csv.reader( source )
    for r in rdr:
        stations.update({r[0]: {'zip':r[1], 'lat':r[3], 'lon':r[2]}})
#pprint.pprint(stations)
#print(stations['2']['zip'])
with open('eligible.json', 'w') as outfile:
    json.dump(stations, outfile)
```

* Long-term, this should be replaced by a direct connection to station connect, 
so that only stations with stale / inactive newscasts get this treatment.

## stitching with ffmpeg:

* track with pauses everywhere you'll cut (but try to say words as if they 
weren't cut) and then trim as tightly as possible with command-t and command-s 
in quicktime.

* create list.txt:

```bash
file '1.mp4'
file '2.mp4'
```

* then run the command:

```bash
ffmpeg -f concat -i list.txt -c copy out.mp4
```


## SOPS
1. Faders down
2. Normal(ize?)
3. Drive: configure YES
4. Record (2x) red
5. If using Skype: 
    * split track
    * faders
    * left/right
6. Open Newsflex
7. Stop
8. Reset faders
 
I think there’s a card in the booths that spells this out more.
 
I brought my own USB thumb drive. But if you know how to save to the network drives and/or navigate Newsflex, you’re probably better off doing it the approved way.